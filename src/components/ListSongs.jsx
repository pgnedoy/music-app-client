import React, { Component } from 'react'
import { Grid } from 'material-ui'
import { withStyles } from 'material-ui/styles';
import List,  { ListItem, ListItemText } from 'material-ui/List';

const styles = theme => ({
  root: {
    background: theme.palette.background.paper,
  },
});

class ListSongs extends Component {
  render() {
    return (
      <Grid item xs={12}>
        <List style={{maxHeight: 460, overflow: 'auto'}}>
          {this.props.list.map((item, index) => 
            (<ListItem  
                button
                key={index}
                onClick={() => { 
                  this.props.song.set(item.name) 
                }}>
                <ListItemText primary={item.name} />
              </ListItem>
            ))}
        </List>
     </Grid>
    );
  }
}

export default withStyles(styles)(ListSongs);