import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import MenuItem from 'material-ui/Menu/MenuItem';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import { Redirect } from 'react-router-dom';

import config from '../config.json'

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  button: {
    margin: theme.spacing.unit + 6,
  },
  
});


class TextFields extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      redirect: false,
      email: '',
      name: '',
      pass: '',
      passConf: ''
    };

    this.handleChange = this.handleChange.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }


  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleClick() {
    fetch(`http://${config.server}:${config.port}/user`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify({
        email: this.state.email, 
        username: this.state.name,
        password: this.state.pass,
        passwordConf: this.state.passConf
      })
    })
    .then(() => {
      this.setState({redirect: true})
    })
  }

  render() {
    const { classes } = this.props;
    if (this.state.redirect) {
      return (<Redirect to='/login'/>)
    }
    return (
      <form className={classes.container} noValidate autoComplete="off">
        <TextField
          id="name"
          label="Name"
          className={classes.textField}
          value={this.state.name}
          onChange={this.handleChange('name')}
          margin="normal"
        />
        <TextField
          id="email"
          label="Email"
          className={classes.textField}
          value={this.state.email}
          onChange={this.handleChange('email')}
          margin="normal"
        />
        <TextField
          id="password"
          label="Password"
          className={classes.textField}
          type="password"
          autoComplete="current-password"
          margin="normal"
          onChange={this.handleChange('pass')}
        />
        <TextField
          id="passwordConf"
          label="Password confim"
          className={classes.textField}
          type="password"
          autoComplete="current-password"
          margin="normal"
          onChange={this.handleChange('passConf')}
        />
        <Button onClick={this.handleClick} className={classes.button} raised color="primary">
          <Icon className={classes.rightIcon}>send</Icon>
        </Button>
      </form>
    );
  }
}

TextFields.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TextFields);