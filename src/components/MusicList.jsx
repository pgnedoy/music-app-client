import React, { Component } from 'react';
import { spring } from 'react-motion';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import List, { ListItem, ListItemText } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import { CardMedia } from 'material-ui/Card';
import Transition from 'react-motion-ui-pack';
import Button from 'material-ui/Button';
import Popover from 'material-ui/Popover';
import Typography from 'material-ui/Typography';

import config from '../config.json'

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: 20,
    backgroundColor: theme.palette.background.paper,
  },
  media: {
    width: 'inherit',
    height: 'inherit'
  },
  mediaBig: {
    width: 500,
    height: 500
  },
  listItem: {
    '&:hover': {
      cursor: 'pointer',
      backgroundColor: '#eff0f1'
    }
  },
  text: {
    paddingTop: 0
  },
  paper: theme.mixins.gutters({
    paddingTop: 8,
    paddingBottom: 8,
    marginTop: theme.spacing.unit * 2,
  }),
  popover: {
    pointerEvents: 'none',
  },
  popperClose: {
    pointerEvents: 'none',
  },
});

class Top extends Component {
  constructor(props){
    super(props)
    this.slice = 15;
    this.state = { slice: this.slice, songs: [], anchorEl: null, popperOpen: false, bigImg: "" }
    this.handleScroll = this.handleScroll.bind(this);
    this.handlePopoverOpen = this.handlePopoverOpen.bind(this);
    this.handlePopoverClose = this.handlePopoverClose.bind(this);
    this.handlePopperOpen = this.handlePopperOpen.bind(this);
    this.handlePopperClose = this.handlePopperClose.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(song, e){
    this.props.song.set(song)
  }

  handlePopoverOpen(link, event){
    this.setState({ anchorEl: event.target, bigImg: link });
  };

  handlePopoverClose(){
    this.setState({ anchorEl: null });
  };

  handlePopperOpen(){
    this.setState({ popperOpen: true });
  };

  handlePopperClose(){
    this.setState({ popperOpen: false });
  };

  handleScroll(event){
    const clientHeight = document.documentElement.clientHeight
    const pageYOffset = window.pageYOffset;
    const scrollHeight = Math.max(
      document.body.scrollHeight,
      document.body.offsetHeight,
      document.body.clientHeight,
    );
    
    if (scrollHeight - clientHeight <= pageYOffset + 10) {
      this.setState({slice: this.state.slice + this.slice})
    }
  }

  init(){
    fetch(`http://${config.server}:${config.port}/song`)
    .then((resp) => resp.json())
    .then((res) => {
      console.log(res)
      this.setState({ songs: res })
    })
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.wasRefreshed !== this.props.wasRefreshed) {
      this.init()
    }
  }

  componentDidMount(){
    window.addEventListener('scroll', this.handleScroll);
    this.init()
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  render(){
    const { anchorEl, popperOpen } = this.state;
    const open = !!anchorEl;
    console.log(this.songs)
    return (
      <div className={this.props.classes.root}>
        <Popover
          className={this.props.classes.popover}
          classes={{
            paper: this.props.classes.paper,
          }}
          open={open}
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}
          onClose={this.handlePopoverClose}
        >
          <CardMedia
            className={this.props.classes.mediaBig}
            image={`${this.state.bigImg}`}
          />
        </Popover>
        <Grid container justify="center">
          <Grid item xs={12}>
            <List>
              {this.state.songs.slice(0, this.state.slice).map((song, index) => {
                console.log(song.path)
                return (
                  <Transition
                    key={song._id}
                    component={false}
                    enter={{
                      opacity: 1,
                      translateY: spring(0, {stiffness: 400, damping: 15})
                    }}
                    leave={{
                      opacity: 0,
                      translateY: 250
                    }}>
                    <Paper onClick={this.handleClick.bind(this, song.path)} key={song._id} className={this.props.classes.paper} elevation={4}>
                      <ListItem  key={song._id} className={this.props.classes.listItem}>
                        <Grid container justify="flex-start" alignContent="center">
                          {/* <Grid item xs={1} >  
                            <Avatar onMouseOver={this.handlePopoverOpen.bind(this, song.link)} onMouseOut={this.handlePopoverClose}>
                              <CardMedia
                                className={this.props.classes.media}
                                image={`${song.link}`}
                              />
                            </Avatar>
                          </Grid>   */}
                          <Grid item xs={12}>  
                            <ListItemText primary={song.title} className={this.props.classes.text}/>
                          </Grid>  
                          {/* <Grid item xs={2}>   */}
                            {/* <ListItemText primary={song.rating} className={this.props.classes.text}/> */}
                          {/* </Grid>   */}
                        </Grid>
                      </ListItem>
                    </Paper>  
                  </Transition>  
                )
              })}
            </List>
          </Grid>  
        </Grid>  
      </div>
    );
  }
}

Top.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Top);