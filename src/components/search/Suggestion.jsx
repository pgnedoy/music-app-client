import React, { Component } from 'react'

import { MenuItem } from 'material-ui/Menu';

function getTime(duration){
  const hr = new RegExp('T(\\d+)')
  const mr = new RegExp('M(\\d+)')
  const sr = new RegExp('S(\\d+)')
  const h = hr.exec(duration)
  const m = mr.exec(duration)
  const s = sr.exec(duration)

  const newstr = `${ (h) ? h["1"] : '00' }:${(m) ? m["1"] : '00'}:${(s) ? s["1"] : '00'}`
  return newstr
}

export default class Suggestion extends Component {
  constructor(props){
    super(props)
    this.state = { title: this.props.suggestion.label, duration: '' }    
  }
  
  getDurationFromYoutube(videoId){
    /*global gapi*/
    const request = gapi && gapi.client.youtube.videos.list(
    {
      part: 'contentDetails',
      id: videoId,
    });
    request.execute((resp) => {
      this.setState({ duration: getTime(resp.items[0].contentDetails.duration) })
    });
  }

  componentDidMount(){
    this.getDurationFromYoutube(this.props.videoId)
  }

  render() {
    const { suggestion, index, itemProps, theme, highlightedIndex, selectedItem } = this.props
    const isHighlighted = highlightedIndex === index
    const isSelected = selectedItem === suggestion.label

    return (
      <MenuItem
        {...itemProps}
        key={this.state.title}
        selected={isHighlighted}
        component="div"
        style={{
          fontWeight: isSelected
            ? theme.typography.fontWeightMedium
            : theme.typography.fontWeightRegular,
        }}
      >
        {`${this.state.title}${(this.state.duration) ? ` - ${this.state.duration}` : ''}`}
      </MenuItem>
    )
  }
}

