import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Grid } from 'material-ui'

import Downshift from 'downshift';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';

import config from '../../config.json'

import Suggestion from './Suggestion'

function renderInput(inputProps) {
  const { classes, autoFocus, value, ref, ...other } = inputProps;
  return (
    <TextField
      autoFocus={autoFocus}
      className={classes.textField}
      value={(value.charAt(0) !== '[') ? value : '' }
      inputRef={ref}
      InputProps={{
        classes: {
          input: classes.input,
        },
        ...other,
      }}
    />
  );
}

function renderSuggestionsContainer(options) {
  const { containerProps, children } = options;

  return (
    <Paper {...containerProps} square>
      {children}
    </Paper>
  );
}

function getSuggestions(suggestions, inputValue) {
  return suggestions
}

const styles = (theme) => ({
  textField: {
    // padding: theme.spacing.unit,
    width: '100%',
  }
});

class IntegrationAutosuggest extends Component {
  constructor(props){
    super(props)
    this.state = { gapiReady: false }
  }

  getItemsFromYoutube(inputValue){
    const request = gapi && gapi.client.youtube.search.list(
    {
      part: 'snippet',
      q: `${inputValue} music`,
      maxResults: 6,
      type: 'video'
    });
    
    request.execute((resp) => {
      const suggestions = resp.items.map(item => ({ 
        label: item.snippet.title, 
        videoId: item.id.videoId,
        image: item.snippet.thumbnails.high.url
       }));
      this.suggestions = suggestions
    });
  }

  loadYoutubeApi() {
    const script = document.createElement("script");
    script.src = "https://apis.google.com/js/client.js"
    const API_KEY = "AIzaSyAyOSj48aaR3PujaFQIMfvh22fRK0HrYs0"
    /*global gapi*/
    script.onload = () => {
      gapi.load('client', () => {
        gapi.client.setApiKey(API_KEY);
        gapi.client.load('youtube', 'v3', () => {
          this.setState({gapiReady: true})
        });
      });
    };

    document.body.appendChild(script);
  }

  componentDidMount() {
    this.loadYoutubeApi();
  }

  handleClick(item) {
    this.props.processing.set(true)
    fetch(`http://${config.server}:${config.port}/song`, 
    {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify({
        youtubeId: item.videoId, 
        title: item.label,
        image: item.image,
      })
    })
      .then((resp) => resp.json())
      .then((song) => {
        console.log('!!!', song)
        this.props.list.update()
        this.props.processing.set(false)
        this.props.song.set(song.path)
      })
  }

  render(){
    const { classes, theme } = this.props
    return this.state.gapiReady ? (
      <Grid container>
        <Grid item xs></Grid>
        <Grid item xs={6}>
          <TextField
              id="search"
              label="Search field"
              type="search"
              className={classes.textField}
              margin="normal"
            />
        </Grid>
        <Grid item xs></Grid>
      </Grid>
    ) : null;
  }
  
}

IntegrationAutosuggest.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(IntegrationAutosuggest);
