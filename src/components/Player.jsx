import React, { Component } from 'react'
import ReactAudioPlayer from 'react-audio-player'
import { Grid } from 'material-ui'

import config from '../config.json'

export default class Player extends Component {
  render() {
    const src = `http://${config.server}:${config.port}/${this.props.song}`
    return (
      <Grid item xs={12} style={{paddingLeft: "175px", marginTop: "20px"}}>
        <ReactAudioPlayer 
          src={src}
          autoPlay
          controls />
      </Grid>
    )
  }
}

