import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import MenuItem from 'material-ui/Menu/MenuItem';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import { Redirect } from 'react-router-dom';

import config from '../config.json'

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  button: {
    margin: theme.spacing.unit + 6,
  },
  
});


class TextFields extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      redirect: false,
      email: '',
      pass: ''
    };

    this.handleChange = this.handleChange.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick() {
    fetch(`http://${config.server}:${config.port}/user`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify({
        logemail: this.state.email, 
        logpassword: this.state.pass,
      })
    })
    .then((resp) => resp.json())
    .then((user) => {
      this.props.setUser(user.username)
      this.setState({redirect: true})
    })
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const { classes } = this.props;
    
    if (this.state.redirect) {
      return (<Redirect to='/'/>)
    }

    return (
      <form className={classes.container} noValidate autoComplete="off">
        <TextField
          id="name"
          label="Email"
          className={classes.textField}
          value={this.state.email}
          onChange={this.handleChange('email')}
          margin="normal"
        />
        <TextField
          id="password"
          label="Password"
          className={classes.textField}
          type="password"
          autoComplete="current-password"
          margin="normal"
          onChange={this.handleChange('pass')}
        />
        <Button onClick={this.handleClick} className={classes.button} raised color="primary">
          <Icon className={classes.rightIcon}>send</Icon>
        </Button>
      </form>
    );
  }
}

TextFields.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TextFields);