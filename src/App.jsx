import React, { Component } from 'react'
import { Grid } from 'material-ui'
import { Switch, Route, Link } from 'react-router-dom';
import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer';
import List from 'material-ui/List';
import Divider from 'material-ui/Divider';

import Player from './components/Player'
import LeftMenu from './components/LeftMenu'
import Login from './components/Login'
import Registration from './components/Registration'
import Search from './components/search/Search'
import MusicList from './components/MusicList'
import ListSongs from './components/ListSongs'
import Progress from './components/Progress'

import config from './config.json'

import AppBar from './components/AppBar';

const styles = theme => ({
  root: {
  },
  list: {
    width: 250,
  }
});

class App extends Component {
  constructor(props) {
    super(props)
    this.state = { 
      song: '',
      list: [],  
      processing: false,
      user: '',
      left: false
    }
    this.setSong = this.setSong.bind(this)
    this.fetchList = this.fetchList.bind(this)
    this.getProcessing = this.getProcessing.bind(this)
    this.setProcessing = this.setProcessing.bind(this)
    this.setUser = this.setUser.bind(this)
    this.unsetUser = this.unsetUser.bind(this)
    this.toggleDrawer = this.toggleDrawer.bind(this)
  }

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  setUser(user){
    this.setState({user})
  }

  unsetUser(){
    this.setState({user: ''})
  }

  getProcessing(){
    return this.state.processing
  }

  setProcessing(processing){
    this.setState({ processing })
  }

  setSong(song){
    this.setState({ song })
  }

  fetchList(){
    fetch(`http://${config.server}:${config.port}/list`)
      .then((resp) => resp.json())
      .then(({ list }) => {
        this.setState({ list })
      })
  }

  componentDidMount(){
    this.fetchList()
  }

  render() {
    const { classes } = this.props;
    
    return (
      <main>
        <AppBar {...{ 
          user: this.state.user,
          setUser: this.setUser,
          unsetUser: this.unsetUser,
          toggleDrawer: this.toggleDrawer
        }} >
        </AppBar>
        <Drawer open={this.state.left} onClose={this.toggleDrawer('left', false)}>
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer('left', false)}
            onKeyDown={this.toggleDrawer('left', false)}
          >
            <LeftMenu/>
          </div>
        </Drawer>
        <Switch>
          <Route exact path='/search' render={(props) => (<Search {...{  
            user: this.state.user, 
            setUser: this.setUser, 
            unsetUser: this.unsetUser, 
            list: { update: this.fetchList }, 
            processing: { get: this.getProcessing, set: this.setProcessing }, 
            song: { set: this.setSong } 
          }}  />)}/>
        </Switch> 
      </main>
    );
  }
}

export default withStyles(styles)(App);
